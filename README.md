# Setup Multiple Static Websites on a Single Server Using Nginx Virtual Hosts



## Getting started

~~~~
Key Concepts Covered
~~~~

1. Subdomains
2. Nginx Virtual Hosts routing.
3. Wildcard SSL (letsencrypt)

**1. Create two subdomains**

I have already purchased the domain **rasulsdevopsprojects.info** and in that project I have created 2 subdomains **project1** and **project2** as A record from DNS Managenent in my GoDaddy account.

![Alt text](images/a_records_for_subdomains.PNG)

**2. Install and configure Nignx on a server**

Firstly, Free Tier EC2 instance has been created with parameters below:

1. Ubuntu 20.04 LTS t2.micro (with key pair for easily connecting and working later!)
2. 8GB volume
3. With new security group
4. HTTP, HTTPS, SSH enabled

![Alt text](images/ec2_ubuntu_instance.PNG)

Allocate and associate Elastic IP for the EC2 instance:

![Alt text](images/elastic_ip.PNG)

**3. Create two website directories with two different website templates.**

`3.1 Let us create the directories for our websites`

~~~~
sudo mkdir -p /var/www/project1.rasulsdevopsprojects.info/html
sudo mkdir -p /var/www/project2.rasulsdevopsprojects.info/html
~~~~

For the website commands above, with -p it creates the parent directories as needed and if exist does not report any error, /var/www/ is the common place to put the content, the last /html is for storing the HTML files.

`3.2 Set the correct permissions for these directories`

The chown command below changes the owner of the files or directories specified. The -R option tells chown to operate on files and directories recursively. So, **_sudo chown -R \$USER:\$USER /var/www/project1.rasulsdevopsprojects.info/html_** means “change the owner of all files and directories under _**/var/www/project1.rasulsdevopsprojects.info/html**_ to the current user”. This is done to ensure that you, as the user setting up the website, have the necessary permissions to modify these files and directories

~~~~
sudo chown -R $USER:$USER /var/www/project1.rasulsdevopsprojects.info/html
sudo chown -R $USER:$USER /var/www/project2.rasulsdevopsprojects.info/html
~~~~

`3.3 Ensure that read access is permitted at the web root and all of the necessary directories`

The chmod command is used to change these permissions. In the command _**sudo chmod -R 755 /var/www**_, 755 means that the owner has read, write, and execute permissions (represented by 7), while the group and others have read and execute permissions (represented by 5). The -R option tells chmod to change permissions recursively, affecting the directory and its contents. This is done to ensure that Nginx (running as a different user for security reasons) has the necessary permissions to access these directories and serve files from them.
So, changing ownership with chown and permissions with chmod is crucial for the proper functioning of your websites and for maintaining system security.

~~~~
sudo chmod -R 755 /var/www
~~~~

`3.4 Create sample index.html files for each site`

~~~~
echo '<html><body><h1>Welcome to Project 1!</h1></body></html>' | sudo tee /var/www/project1.rasulsdevopsprojects.info/html/index.html
echo '<html><body><h1>Welcome to Project 2!</h1></body></html>' | sudo tee /var/www/project2.rasulsdevopsprojects.info/html/index.html
~~~~

**4. Configure the Virtual host to point two subdomains to two different website directories**

`4.1 Create a new configuration file for each of your sites`

~~~~
sudo vim /etc/nginx/sites-available/project1.rasulsdevopsprojects.info
sudo vim /etc/nginx/sites-available/project2.rasulsdevopsprojects.info
~~~~

`4.2 Add a server block to each file` 

Here’s an example of what you might put in project1.rasulsdevopsprojects.info:

~~~~
server {
    listen 80;
    listen [::]:80;

    root /var/www/project1.rasulsdevopsprojects.info/html;
    index index.html;

    server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;

    location / {
        try_files $uri $uri/ =404;
    }
}
~~~~

And here’s an example for project2.rasulsdevopsprojects.info:

~~~~
server {
    listen 80;
    listen [::]:80;

    root /var/www/project2.rasulsdevopsprojects.info/html;
    index index.html;

    server_name project2.rasulsdevopsprojects.info www.project2.rasulsdevopsprojects.info;

    location / {
        try_files $uri $uri/ =404;
    }
}
~~~~

Config files above are a server block configuration for Nginx, which is used to define how to respond to requests for a specific domain. Here’s what each part does:

- _listen 80; and listen [::]:80;_ : These lines tell Nginx to listen on port 80, the default port for HTTP, for both IPv4 and IPv6 connections.

- _root /var/www/project1.rasulsdevopsprojects.info/html;_ : This line sets the root directory for requests, which is where Nginx will look for files to serve for this site.

- _index index.html;_ : This line tells Nginx to serve index.html as the default file for requests to the site.

- _server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;_ : This line sets the domain names that this server block will respond to.

- _location / {...}_ : This block tells Nginx what to do with requests for different URLs. In this case, it’s set up to try serving the requested URL as a file or directory ($uri or $uri/), and if neither of those exist, it will return a 404 error.

So, in summary, this server block is set up to serve files from _**/var/www/project1.rasulsdevopsprojects.info/html**_ in response to requests for '_**`project1.rasulsdevopsprojects.info`**_' or '_**`www.project1.rasulsdevopsprojects.info`**_', with index.html as the default file, and it will return a 404 error if a requested file or directory doesn’t exist. 

`4.3 Enable your sites`

~~~~
sudo ln -s /etc/nginx/sites-available/project1.rasulsdevopsprojects.info /etc/nginx/sites-enabled/
sudo ln -s /etc/nginx/sites-available/project2.rasulsdevopsprojects.info /etc/nginx/sites-enabled/
~~~~

These commands are creating symbolic links (or symlinks) from the `sites-available` directory to the `sites-enabled` directory for each of your sites. 

Here's a breakdown:

- `sudo ln -s`: The command is used to create links between files with super user. The `-s` option tells it to create a symbolic link, which is a type of file that points to another file or directory¹.

- `/etc/nginx/sites-available/project1.rasulsdevopsprojects.info`: This is the source file, or the file that the symlink will point to. It's the configuration file for your site in the `sites-available` directory.

- `/etc/nginx/sites-enabled/`: This is the target directory, or where the symlink will be created. The `sites-enabled` directory is where Nginx looks to see which sites are enabled¹.

So, in summary, these commands are creating symlinks that point from the `sites-enabled` directory to the configuration files in the `sites-available` directory. This is a common setup that allows you to enable and disable sites by creating and removing symlinks, without having to edit the actual configuration files.

`4.4 Test your configuration`

~~~~
sudo nginx -t
~~~~

`4.5 Restart nginx`

~~~~
sudo systemctl restart nginx
~~~~

Now, if you navigate to project1.rasulsdevopsprojects.info or project2.rasulsdevopsprojects.info in a web browser, you should see the respective welcome pages.

**5. Validate the setup accessing the subdomains.**

Enter the URL of your subdomains: In the address bar of your web browser, type `http://project1.rasulsdevopsprojects.info` and press Enter. You should see the “Welcome to Project 1!” page that you set up earlier. Do the same for `http://project2.rasulsdevopsprojects.info` and you should see the “Welcome to Project 2!” page.

Sorry,  I have taken the screenshots after configuring the HTTPS but, in your case it will open just with HTTP 🙂

![Alt text](images/validate_subdomain_1.PNG)
![Alt text](images/validate_subdomain_2.PNG)

Please note that it may take some time (up to 24-48 hours) for DNS changes to propagate across the internet, so if you’ve just set up the A records for your subdomains, you might not be able to access them immediately but for me it worked almost immediately.

**6. Create a wildcard Letsencrypt SSL certificate for the root Domain**

`6.1 Install Certbot and the Nginx plugin`

~~~~
sudo apt install certbot python3-certbot-nginx
~~~~

`6.2 Obtain the wildcard certificate`

~~~~
sudo certbot certonly --manual --preferred-challenges=dns --server https://acme-v02.api.letsencrypt.org/directory --agree-tos -d *.rasulsdevopsprojects.info
~~~~

This command will start the process of obtaining the certificate but will pause and provide instructions on how to set a DNS TXT record for your domain.

`6.3 Set the DNS TXT record` 

Log into your DNS provider (GoDaddy) and add a TXT record with the name and value provided by Certbot.`

`6.4 Verify and install the certificate` 

After setting the DNS record, press Enter in the terminal where Certbot is running to continue the process. If the verification is successful, Certbot will provide instructions on how to install the certificate.

Please note that wildcard certificates require DNS validation, so you must be able to add a DNS TXT record to your domain’s DNS settings.


**7. Configure wildcard SSL on Nginx for two websites.**

`7.1 Open the Nginx configuration file for your first site`

~~~~
sudo vim /etc/nginx/sites-available/project1.rasulsdevopsprojects.info
~~~~

`7.2 Modify the server block to include the SSL certificate`

Here’s an example of what you might put in project1.rasulsdevopsprojects.info:

~~~~
server {
    listen 80;
    listen [::]:80;
    listen 443 ssl; # add this line
    listen [::]:443 ssl; # and this line

    ssl_certificate /etc/letsencrypt/live/rasulsdevopsprojects.info/fullchain.pem; # path to your cert
    ssl_certificate_key /etc/letsencrypt/live/rasulsdevopsprojects.info/privkey.pem; # path to your private key

    root /var/www/project1.rasulsdevopsprojects.info/html;
    index index.html;

    server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;

    location / {
        try_files $uri $uri/ =404;
    }
}
~~~~

`7.3 Repeat steps a and b for your second site project2.rasulsdevopsprojects.info`

`7.4 Test your configuration`

~~~~
sudo nginx -t
~~~~

`7.5 Restart Nginx`

~~~~
sudo systemctl restart nginx
~~~~

Now, if you navigate to `https://project1.rasulsdevopsprojects.info` or `https://project2.rasulsdevopsprojects.info` in a web browser, you should see the respective welcome pages served over HTTPS.

_To automatically redirect HTTP traffic to HTTPS, you can add a new server block to your Nginx configuration files that listens on port 80 (the default HTTP port) and redirects all traffic to the HTTPS version of the site. Here’s how you can do it:_

_a. Open the Nginx configuration file for your first site:_

~~~~
sudo vim /etc/nginx/sites-available/project1.rasulsdevopsprojects.info
~~~~

_b. Add a new server block at the top of the file:_

~~~~
server {
    listen 80;
    listen [::]:80;
    server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;
    return 301 https://$host$request_uri;
}
~~~~

_This is a server block in the Nginx configuration file that sets up a redirect from HTTP to HTTPS for your website. Here's what each part does:_

- `listen 80;` and `listen [::]:80;`: These lines tell Nginx to listen on port 80, the default port for HTTP, for both IPv4 and IPv6 connections.

- `server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;`: This line sets the domain names that this server block will respond to. You should replace `project1.rasulsdevopsprojects.info` and `www.project1.rasulsdevopsprojects.info` with your actual domain names.

- `return 301 https://$host$request_uri;`: This line tells Nginx to send a 301 redirect to any requests that come to this server block, redirecting them to the HTTPS version of the requested URL. `$host` is a variable that Nginx automatically sets to the hostname of the server, and `$request_uri` is the URI of the request.

_So, you only need to replace `project1.rasulsdevopsprojects.info` and `www.project1.rasulsdevopsprojects.info` with your actual domain names. The rest of the lines can be left as they are._


_**FOR EXAMPLE:**_

~~~~
server {
    listen 80;
    listen [::]:80;
    server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    ssl_certificate /etc/letsencrypt/live/rasulsdevopsprojects.info/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/rasulsdevopsprojects.info/privkey.pem;

    root /var/www/project1.rasulsdevopsprojects.info/html;
    index index.html;

    server_name project1.rasulsdevopsprojects.info www.project1.rasulsdevopsprojects.info;

    location / {
        try_files $uri $uri/ =404;
    }
}
~~~~

_c. Repeat steps a and b for your second site_
_d. Test your configuration:_

~~~~
sudo nginx -t
~~~~

_e. Restart Nginx:_

~~~~
sudo systemctl restart nginx
~~~~

_Now, if you navigate to `http://project1.rasulsdevopsprojects.info` or `http://project2.rasulsdevopsprojects.info` in a web browser, you should be automatically redirected to the HTTPS version of the site._

**8. Validate the subdomain websites’ SSL using OpenSSL utility.**

8.1 Run the OpenSSL command for your first site:

~~~~
echo | openssl s_client -servername project1.rasulsdevopsprojects.info -connect project1.rasulsdevopsprojects.info:443 2>/dev/null | openssl x509 -noout -dates
~~~~

8.2 Run the OpenSSL command for your second site:

~~~~
echo | openssl s_client -servername project2.rasulsdevopsprojects.info -connect project2.rasulsdevopsprojects.info:443 2>/dev/null | openssl x509 -noout -dates
~~~~

These commands will connect to your sites using OpenSSL, retrieve the SSL certificates, and then print out the notBefore and notAfter dates of the certificates. This allows you to verify that the certificates are valid and check their expiration dates.








